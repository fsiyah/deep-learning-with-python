# 4 Fundamentals of machine learning

## 4.1 Four branches of machine learning

- Supervised learning
- Unsupervised learning
- Self-supervised learning
- Reinforcement learning

### 4.1.1 Supervised learning
 It consists of learning to map input data to
known targets (also called annotations), given a set of examples (often annotated by humans).

- Sequence generation: Given a picture, predict a caption describing it. Sequence
generation can sometimes be reformulated as a series of classification problems
(such as repeatedly predicting a word or token in a sequence).
- Syntax tree prediction: Given a sentence, predict its decomposition into a syntax
tree.
- Object detection: Given a picture, draw a bounding box around certain objects
inside the picture. This can also be expressed as a classification problem (given
many candidate bounding boxes, classify the contents of each one) or as a joint
classification and regression problem, where the bounding-box coordinates are
predicted via vector regression.
- Image segmentation: Given a picture, draw a pixel-level mask on a specific object

### 4.1.2 Unsupervised learning
This branch of machine learning consists of finding interesting transformations of the
input data without the help of any targets, for the purposes of data visualization, data
compression, or data denoising, or to better understand the correlations present in
the data at hand

- Dimensionality reduction 
- Clustering 

are well-known categories of unsupervised learning. 

### 4.1.3 Self-supervised learning
Self-supervised learning is supervised learning without human-annotated labels—you can think of it as supervised learning without any
humans in the loop. There are still labels involved (because the learning has to be supervised by something), but they’re generated from the input data, typically using a heuristic algorithm.

- Trying to predict the next frame in a video, given past frames, or the next word in a text, given previous words, are instances of self-supervised learning (temporally supervised learning, in this case: supervision comes from future input data).


### 4.1.4 Reinforcement learning

In reinforcement learning,
an agent receives information about its environment and learns to choose actions that
will maximize some reward. For instance, a neural network that “looks” at a videogame screen and outputs game actions in order to maximize its score can be trained
via reinforcement learning.

## Classification and regression glossary
Classification and regression involve many specialized terms. You’ve come across
some of them in earlier examples, and you’ll see more of them in future chapters.
They have precise, machine-learning-specific definitions, and you should be familiar
with them:
- Sample or input—One data point that goes into your model.
- Prediction or output—What comes out of your model.
- Target—The truth. What your model should ideally have predicted, according
to an external source of data.
- Prediction error or loss value—A measure of the distance between your
model’s prediction and the target.
- Classes—A set of possible labels to choose from in a classification problem.
For example, when classifying cat and dog pictures, “dog” and “cat” are the
two classes.
- Label—A specific instance of a class annotation in a classification problem.
For instance, if picture #1234 is annotated as containing the class “dog,”
then “dog” is a label of picture #1234.
- Ground-truth or annotations—All targets for a dataset, typically collected by
humans.
- Binary classification—A classification task where each input sample should
be categorized into two exclusive categories.
- Multiclass classification—A classification task where each input sample
should be categorized into more than two categories: for instance, classifying
handwritten digits.
- Multilabel classification—A classification task where each input sample can
be assigned multiple labels. For instance, a given image may contain both a
cat and a dog and should be annotated both with the “cat” label and the
“dog” label. The number of labels per image is usually variable.
- Scalar regression—A task where the target is a continuous scalar value. Predicting house prices is a good example: the different target prices form a continuous space.
- Vector regression—A task where the target is a set of continuous values: for
example, a continuous vector. If you’re doing regression against multiple values (such as the coordinates of a bounding box in an image), then you’re
doing vector regression.
- Mini-batch or batch—A small set of samples (typically between 8 and 128)
that are processed simultaneously by the model. The number of samples is
often a power of 2, to facilitate memory allocation on GPU. When training, a
mini-batch is used to compute a single gradient-descent update applied to
the weights of the model. 

## 4.2 Evaluating machine-learning models

### 4.2.1 Training, validation, and test sets
#### SIMPLE HOLD-OUT VALIDATION
#### K-FOLD VALIDATION
#### ITERATED K-FOLD VALIDATION WITH SHUFFLING

### 4.2.2 Things to keep in mind
Keep an eye out for the following when you’re choosing an evaluation protocol:
- **Data representativeness:** You want both your training set and test set to be representative of the data at hand. For instance, if you’re trying to classify images of
digits, and you’re starting from an array of samples where the samples are
ordered by their class, taking the first 80% of the array as your training set and
the remaining 20% as your test set will result in your training set containing
only classes 0–7, whereas your test set contains only classes 8–9. This seems like
a ridiculous mistake, but it’s surprisingly common. For this reason, you usually
should randomly shuffle your data before splitting it into training and test sets.
- **The arrow of time:** If you’re trying to predict the future given the past (for example, tomorrow’s weather, stock movements, and so on), you should not randomly shuffle your data before splitting it, because doing so will create a
temporal leak: your model will effectively be trained on data from the future. In
such situations, you should always make sure all data in your test set is posterior
to the data in the training set.
- **Redundancy in your data:** If some data points in your data appear twice (fairly
common with real-world data), then shuffling the data and splitting it into a
training set and a validation set will result in redundancy between the training
and validation sets. In effect, you’ll be testing on part of your training data,
which is the worst thing you can do! Make sure your training set and validation
set are disjoint. 

## 4.3 Data preprocessing, feature engineering, and feature learning

### 4.3.1 Data preprocessing for neural networks

#### VECTORIZATION
All inputs and targets in a neural network must be tensors of floating-point data (or, in specific cases, tensors of integers). 
We used one-hot encoding to turn them into a tensor of float32 data in chapter-3 word classification example.
#### VALUE NORMALIZATION
Before you fed this data into your network, you had to cast it to float32 and divide by 255 so you’d end up with floatingpoint values in the 0–1 range.

- Take small values—Typically, most values should be in the 0–1 range.
- Be homogenous—That is, all features should take values in roughly the same
range.

Additionally,

- Normalize each feature independently to have a mean of 0.
- Normalize each feature independently to have a standard deviation of 1.
#### HANDLING MISSING VALUES
In general, with neural networks, it’s safe to input missing values as 0, with the condition that 0 isn’t already a meaningful value.
### 4.3.2 Feature engineering
![feature-engineering](feature-engineering.PNG)
## 4.4 Overfitting and underfitting
### 4.4.1 Reducing the network’s size
The simplest way to prevent overfitting is to reduce the size of the model: the number of learnable parameters in the model (which is determined by the number of layers and the number of units per layer).
Decreasing the memorizaation capacity of the network.
### 4.4.2 Adding weight regularization
A simple model in this context is a model where the distribution of parameter values has less entropy (or a model with fewer parameters, as you saw in the previous section). Thus a common way to mitigate overfitting is to put constraints on the complexity of a network by forcing its weights to take only small values, which makes the distribution of weight values more regular. This is called weight regularization, and it’s done by adding to the loss function of the network a cost associated with having large weights. This cost comes in two flavors:
- **L1 regularization:** The cost added is proportional to the absolute value of the
weight coefficients (the L1 norm of the weights).
- **L2 regularization:** The cost added is proportional to the square of the value of the weight coefficients (the L2 norm of the weights). L2 regularization is also called weight decay in the context of neural networks. Don’t let the different name confuse you: weight decay is mathematically the same as L2 regularization.
### 4.4.3 Adding dropout
![dropout](dropout.PNG)
## 4.5 The universal workflow of machine learning
### 4.5.1 Defining the problem and assembling a dataset
### 4.5.2 Choosing a measure of success
### 4.5.3 Deciding on an evaluation protocol
### 4.5.4 Preparing your data
### 4.5.5 Developing a model that does better than a baseline
### 4.5.6 Scaling up: developing a model that overfits
### 4.5.7 Regularizing your model and tuning your hyperparameters
