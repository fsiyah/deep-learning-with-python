# 1. Fundamentals of deep learning

# 1.1 Artifficial Intelligence, machine learning and dep learning

### 1.1.1 Artifficial Intelligence
For a fairly long time, many experts believed that human-level artificial intelligence could be achieved by having programmers handcraft a sufficiently large set of explicit rules for manipulating knowledge. This approach is known as symbolic AI.

Although symbolic AI proved suitable to solve well-defined, logical problems, such as playing chess, it turned out to be intractable to figure out explicit rules for solving more complex, fuzzy problems, such as image classification, speech recognition, and language translation. A new approach arose to take symbolic AI’s place: machine learning.

> Deep Learning < Machine Learning < Artifficial Intelligence

### 1.1.2 Machine Learning

> Classical Programming : Rules + Data -> Answers
>
> Machine Learning : Data + Answers -> Rules

A machine-learning systeemm is trained rather than explicitly programmed.

### 1.1.3 Learning Representations from data (Differences between Machine Learning and Deep Learning)

Machine learning discovers the relationship between given-data an expected outcomes.

Machine learning need three things:
1. Input data points.
2. Examples of the expected outputs.
3. A way to measure whether the algorithm is doing a good job or not.
    According to this measurement, machine learning algorithm should adjust its way of working. Which is called "learning" phase.

> There is a central problem for both machine learning and deep learning that meaningfully transform data or learn useful representations of the input data.

### 1.1.4 The "deep" in deep learning

The deep in deep learning isn’t a reference to any kind of deeper understanding achieved by the approach; rather, it stands for this idea of successive layers of representations.

How many layers contribute to a model of the data is called the "depth" of the model.

In deep learning, these layered representations are (almost always) learned via models called neural networks, structured in literal layers stacked on top of each other.

You can think of a deep network as a multistage
information-distillation operation, where information goes through successive filters
and comes out increasingly purified (that is, useful with regard to some task).
So that’s what deep learning is, technically: a multistage way to learn data representations. It’s a simple idea—but, as it turns out, very simple mechanisms, sufficiently
scaled, can end up looking like magic. 

### 1.1.5 Understanding how deep learning works

The specification of how a layer behave to its input data is weights of layer (parameters of layer). 

To control the output of a neural network, you need to be able to measure how far this output is from what you expected. This is the job of the loss function of the network, also called the objective function.

The fundamental trick in deep learning is to use this score as a feedback signal to
adjust the value of the weights a little, in a direction that will lower the loss score for
the current example. This adjustment is the job of the optimizer, which implements what’s called the Backpropagation algorithm: the central algorithm in deep learning. 
