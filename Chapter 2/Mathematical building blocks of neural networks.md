# 2. Mathematical building blocks of neural networks

## 2.2 Data repesentation for neural networks
  ![Representation of tensors with different diensions](tensors.png)

## 2.3 The gears of neural networks: tensor operations
### 2.3.6 A geometric interpretation of deep learning

You just learned that neural networks consist entirely of chains of tensor operations and
that all of these tensor operations are just geometric transformations of the input data.
It follows that you can interpret a neural network as a very complex geometric transformation in a high-dimensional space, implemented via a long series of simple steps.
 In 3D, the following mental image may prove useful. Imagine two sheets of colored
paper: one red and one blue. Put one on top of the other. Now crumple them
together into a small ball. That crumpled paper ball is your input data, and each sheet
of paper is a class of data in a classification problem. What a neural network (or any
other machine-learning model) is meant to do is figure out a transformation of the
paper ball that would uncrumple it, so as to make the two classes cleanly separable
again. With deep learning, this would be implemented as a series of simple transformations of the 3D space, such as those you could apply on the paper ball with your fingers, one movement at a time.

Uncrumpling paper balls is what machine learning is about: finding neat representations for complex, highly folded data manifolds. At this point, you should have a
pretty good intuition as to why deep learning excels at this: it takes the approach of
incrementally decomposing a complicated geometric transformation into a long
chain of elementary ones, which is pretty much the strategy a human would follow to
uncrumple a paper ball. Each layer in a deep network applies a transformation that
disentangles the data a little—and a deep stack of layers makes tractable an extremely
complicated disentanglement process. 

## 2.4 The engine of neural networks: gradient-based optimization

One naive solution would be to freeze all weights in the network except the one
scalar coefficient being considered, and try different values for this coefficient.

Let’s
say the initial value of the coefficient is 0.3. After the forward pass on a batch of data,
the loss of the network on the batch is 0.5. If you change the coefficient’s value to 0.35
and rerun the forward pass, the loss increases to 0.6. But if you lower the coefficient to
0.25, the loss falls to 0.4. In this case, it seems that updating the coefficient by -0.05 would contribute to minimizing the loss. This would have to be repeated for all coefficients in the network.


But such an approach would be horribly inefficient, because you’d need to compute two forward passes (which are expensive) for every individual coefficient (of
which there are many, usually thousands and sometimes up to millions).

### 2.4.3 Stochastic gradient descent

1. Draw a batch of training samples x and corresponding targets y.
2. Run the network on x to obtain predictions y_pred.
3. Compute the loss of the network on the batch, a measure of the mismatch
between y_pred and y.
4. **Compute the gradient of the loss with regard to the network’s parameters (a
backward pass).**
5. Move the parameters a little in the opposite direction from the gradient—for
example W -= step * gradient—thus reducing the loss on the batch a bit

>The term stochastic refers to the fact that each batch of data is drawn at
random (stochastic is a scientific synonym of random). 

As you can see, intuitively it’s important to pick a reasonable value for the step factor.
- If it’s too small, the descent down the curve will take many iterations, and it could get
stuck in a local minimum. 
- If step is too large, your updates may end up taking you to
completely random locations on the curve.

To solve stucking local minima issue, the momentum concept is used. 

In practice, this means updating the parameter w based not only on the current gradient value but also on the previous parameter update.
