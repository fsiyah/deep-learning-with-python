# 3. Getting started with neural networks

- Classifying movie reviews as positive or negative (binary classification) 
- Classifying news wires by topic (multiclass classification) 
- Estimating the price of a house, given real-estate data (regression)

## 3.1 Anatomy of a network
![relationship-between-layers-lossfunction-optimizer](relationship-between-layers-lossfunction-optimizer.PNG)

### 3.1.3 Loss functions and optimizers: keys to configuring the learning process

- Loss function (objective function)—The quantity that will be minimized during
training. It represents a measure of success for the task at hand.
- Optimizer—Determines how the network will be updated based on the loss function. It implements a specific variant of stochastic gradient descent (SGD)

A neural network that has multiple outputs may have multiple loss functions (one per
output). But the gradient-descent process must be based on a single scalar loss value;
so, for multiloss networks, all losses are combined (via averaging) into a single scalar
quantity.

Fortunately, when it comes to common problems such as classification, regression,
and sequence prediction, there are simple guidelines you can follow to choose the
correct loss. For instance, you’ll use binary crossentropy for a two-class classification problem, categorical crossentropy for a many-class classification problem, meansquared error for a regression problem, connectionist temporal classification (CTC) for a sequence-learning problem, and so on. 

> Only when you’re working on truly new research problems will you have to develop your own objective functions. In the next few chapters, we’ll detail explicitly which loss functions to choose for a wide range of common tasks. 